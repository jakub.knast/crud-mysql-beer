package com.jk.crudmysqlbeer;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;


@EnableDiscoveryClient
@SpringBootApplication
public class CrudMysqlBeerApplication {

    public static void main(String[] args) {
        SpringApplication.run(CrudMysqlBeerApplication.class, args);
    }

}
