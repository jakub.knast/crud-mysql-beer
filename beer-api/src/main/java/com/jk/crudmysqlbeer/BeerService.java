package com.jk.crudmysqlbeer;

import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class BeerService {
    private final BeerRepository beerRepository;

    public List<BeerEntity> findAll() {
        return beerRepository.findAll();
    }

    public Optional<BeerEntity> findById(Long id) {
        return beerRepository.findById(id);
    }

    public BeerEntity save(BeerEntity stock) {
        return beerRepository.save(stock);
    }

    public void deleteById(Long id) {
        beerRepository.deleteById(id);
    }
}
