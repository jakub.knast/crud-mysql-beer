package com.jk.crudmysqlbeer;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api/v1/beers")
@CrossOrigin
@Slf4j
@RequiredArgsConstructor
public class BeerController {
    private final BeerService beerService;

    @GetMapping
    public ResponseEntity<List<BeerEntity>> findAll() {
        return ResponseEntity.ok(beerService.findAll());
    }

    @PostMapping
    public ResponseEntity create(@Valid @RequestBody BeerEntity BeerEntity) {
        return ResponseEntity.ok(beerService.save(BeerEntity));
    }

    @GetMapping("/{id}")
    public ResponseEntity<BeerEntity> findById(@PathVariable Long id) {
        Optional<BeerEntity> stock = beerService.findById(id);
        if (!stock.isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(stock.get());
    }

    @PutMapping("/{id}")
    public ResponseEntity<BeerEntity> update(@PathVariable Long id, @Valid @RequestBody BeerEntity BeerEntity) {
        if (!beerService.findById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        return ResponseEntity.ok(beerService.save(BeerEntity));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity delete(@PathVariable Long id) {
        if (!beerService.findById(id).isPresent()) {
            log.error("Id " + id + " is not existed");
            ResponseEntity.badRequest().build();
        }

        beerService.deleteById(id);

        return ResponseEntity.ok().build();
    }
}
