Hello on beer project. 

This is a simple client server project.
Client app is a web based Angular project.
Server app is a RESTful app made with spring boot REST.
Data are marshaled with hibernate and stored in mysql 5.7

Go to http://3.127.29.161/beers to see running project

There two ways of run this project: dev or prod with dockers
Dev:
1. Install mysql 5.7 and run mysql on port 3306, user root without password - either configure that connection in `... beer-api/src/main/resource/application.yaml` (then rebuild image)
2. build beer-api with maven: `mvn clean install`
3. instal node (some new version), install angular-cli with `npm install -g @angular/cli`, then run beer-ui with `ng serve`. It starts on localhost:4200

Prod:
1. Install docker on your machine or use cloud
2. Run `docker-compose up` to start all three images.


Docker Images
beer-api and beer-ui images are availabe on my dockerhub repo:
 - https://hub.docker.com/repository/docker/jakubknast/beer-api
 - https://hub.docker.com/repository/docker/jakubknast/beer-ui
mysql is official mysql docker image

In api and ui directories are build-&ast;.sh scripts to create dockers
If somehow you dont like docker-compose you can manually install (docker run) all images using start-&ast;.sh scripts located in components directores. Remember to run db first.

```
docker run -dti --rm --network net --name my -e MYSQL_ALLOW_EMPTY_PASSWORD=true -p 3306:3306 mysql:5.7
docker run -dti --network net --name ba -p 8000:8000 jakubknast/beer-api
docker run -dti --network net --name bu -p 8888:80 jakubknast/beer-ui
```

Go to http://3.127.29.161/beers to see running project

