#!/bin/bash
docker run -dit --network net --name db -p 3306:3306 -e MYSQL_DATABASE=test -e MYSQL_USER=test -e MYSQL_PASSWORD=test -e MYSQL_ROOT_PASSWORD=test quay.io/perriea/alpine-mysql

#docker run -dti --rm --network net --name my -e MYSQL_ALLOW_EMPTY_PASSWORD=true -p 3306:3306 mysql:5.7
