import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Beer} from '../model/beer';
import {BeerService} from '../service/beer.service';

@Component({
  selector: 'app-update-beer',
  templateUrl: './update-beer.component.html',
  styleUrls: ['./update-beer.component.css']
})
export class UpdateBeerComponent {

  beer: Beer;

  constructor(private route: ActivatedRoute, private router: Router, private beerService: BeerService) {
    this.beer = new Beer();
  }

  onSubmit() {
    this.beerService.update(this.beer).subscribe(result => this.gotoUserList());
  }

  gotoUserList() {
    this.router.navigate(['/beers']);
  }


  loadBeerById(id: string) {
    this.beerService.findById(id).subscribe(data => {
      this.beer = data;
    }, error => {
      const tmp = new Beer();
      tmp.id = id;
      this.beer = tmp;
    });
  }
}
