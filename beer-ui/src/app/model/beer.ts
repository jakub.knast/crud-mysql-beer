export class Beer {
  id: string;
  name: string;
  percentage: number;
  price: number;

}
