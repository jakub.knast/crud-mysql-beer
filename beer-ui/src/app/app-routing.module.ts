import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {BeerListComponent} from './beer-list/beer-list.component';
import {BeerFormComponent} from './beer-form/beer-form.component';
import {UpdateBeerComponent} from './update-beer/update-beer.component';

const routes: Routes = [
  { path: 'beers', component: BeerListComponent},
  { path: 'addbeer', component: BeerFormComponent },
  { path: 'updateBeer', component: UpdateBeerComponent }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
